***************************
** DIGITAL SYSTEMS LAB 4 **
****** ASSIGNMENT 3 *******
********* GROUP C *********
***************************

This short document serves to explain the file structure of this submission.

- The main folder contains all the provided system files (Processor, RAM, ROM,
ALU, Timer), the System top level module, the constraints file, as well as the
actual RAM and ROM memory files.

- The IR folder contains all files pertaining to the infrared peripheral.

- The Mouse folder contains all files pertaining to the mouse peripheral.

- The VGA folder contains all files pertaining to the VGA peripheral.

- The LED_7seg folder contains all files pertaining to the 7-segment display
and the LED peripherals.

- The gen folder contains the program generator, the assembler, and the Assembly
representation of the ROM contents.
