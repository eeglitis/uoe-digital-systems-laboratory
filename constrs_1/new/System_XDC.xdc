# Clock and reset signals
set_property PACKAGE_PIN W5 [get_ports CLK]
    set_property IOSTANDARD LVCMOS33 [get_ports CLK]
set_property PACKAGE_PIN T17 [get_ports RESET]
    set_property IOSTANDARD LVCMOS33 [get_ports RESET]
    
# VGA Red signals
set_property PACKAGE_PIN N19 [get_ports {VGA_OUT[11]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[11]}]
set_property PACKAGE_PIN J19 [get_ports {VGA_OUT[10]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[10]}]
set_property PACKAGE_PIN H19 [get_ports {VGA_OUT[9]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[9]}]
set_property PACKAGE_PIN G19 [get_ports {VGA_OUT[8]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[8]}]

# VGA Green signals    
set_property PACKAGE_PIN D17 [get_ports {VGA_OUT[7]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[7]}]
set_property PACKAGE_PIN G17 [get_ports {VGA_OUT[6]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[6]}]
set_property PACKAGE_PIN H17 [get_ports {VGA_OUT[5]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[5]}]
set_property PACKAGE_PIN J17 [get_ports {VGA_OUT[4]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[4]}]

# VGA Blue signals    
set_property PACKAGE_PIN J18 [get_ports {VGA_OUT[3]}]      
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[3]}]
set_property PACKAGE_PIN K18 [get_ports {VGA_OUT[2]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[2]}]
set_property PACKAGE_PIN L18 [get_ports {VGA_OUT[1]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[1]}]
set_property PACKAGE_PIN N18 [get_ports {VGA_OUT[0]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {VGA_OUT[0]}]

# Horizontal and vertical sync signals    
set_property PACKAGE_PIN P19 [get_ports VGA_HS]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_HS]
set_property PACKAGE_PIN R19 [get_ports VGA_VS]
    set_property IOSTANDARD LVCMOS33 [get_ports VGA_VS]

# First 4 switches (car select)
set_property PACKAGE_PIN V17 [get_ports {CAR_SELECT[0]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {CAR_SELECT[0]}]
set_property PACKAGE_PIN V16 [get_ports {CAR_SELECT[1]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {CAR_SELECT[1]}]
set_property PACKAGE_PIN W16 [get_ports {CAR_SELECT[2]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {CAR_SELECT[2]}]
set_property PACKAGE_PIN W17 [get_ports {CAR_SELECT[3]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {CAR_SELECT[3]}]

# IR signal
set_property PACKAGE_PIN B16 [get_ports IR_LED]
    set_property IOSTANDARD LVCMOS33 [get_ports IR_LED]

#7SEGMENT constraints   
#7SEG Anodes
set_property PACKAGE_PIN U2 [get_ports SEG_SELECT[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports SEG_SELECT[0]]
set_property PACKAGE_PIN U4 [get_ports SEG_SELECT[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports SEG_SELECT[1]]
set_property PACKAGE_PIN V4 [get_ports SEG_SELECT[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports SEG_SELECT[2]]
set_property PACKAGE_PIN W4 [get_ports SEG_SELECT[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports SEG_SELECT[3]]

#7SEG Cathodes
set_property PACKAGE_PIN W7 [get_ports DEC_OUT[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[0]]
set_property PACKAGE_PIN W6 [get_ports DEC_OUT[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[1]]
set_property PACKAGE_PIN U8 [get_ports DEC_OUT[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[2]]
set_property PACKAGE_PIN V8 [get_ports DEC_OUT[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[3]]
set_property PACKAGE_PIN U5 [get_ports DEC_OUT[4]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[4]]
set_property PACKAGE_PIN V5 [get_ports DEC_OUT[5]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[5]]
set_property PACKAGE_PIN U7 [get_ports DEC_OUT[6]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[6]]
set_property PACKAGE_PIN V7 [get_ports DEC_OUT[7]]
    set_property IOSTANDARD LVCMOS33 [get_ports DEC_OUT[7]]
    
#Mouse interface_
set_property PACKAGE_PIN C17 [get_ports PS2_CLK]
    set_property IOSTANDARD LVCMOS33 [get_ports PS2_CLK]
    set_property PULLUP true [get_ports PS2_CLK]
set_property PACKAGE_PIN B17 [get_ports PS2_DATA] 
    set_property IOSTANDARD LVCMOS33 [get_ports PS2_DATA]
    set_property PULLUP true [get_ports PS2_DATA]
    
#LEDS 
set_property PACKAGE_PIN U16 [get_ports LED_OUT[0]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[0]]   
set_property PACKAGE_PIN E19 [get_ports LED_OUT[1]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[1]] 
set_property PACKAGE_PIN U19 [get_ports LED_OUT[2]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[2]]   
set_property PACKAGE_PIN V19 [get_ports LED_OUT[3]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[3]]
set_property PACKAGE_PIN W18 [get_ports LED_OUT[4]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[4]]   
set_property PACKAGE_PIN U15 [get_ports LED_OUT[5]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[5]] 
set_property PACKAGE_PIN U14 [get_ports LED_OUT[6]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[6]]   
set_property PACKAGE_PIN V14 [get_ports LED_OUT[7]]
    set_property IOSTANDARD LVCMOS33 [get_ports LED_OUT[7]] 
