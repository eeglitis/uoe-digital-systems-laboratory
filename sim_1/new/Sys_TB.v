`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2017 14:51:56
// Design Name: 
// Module Name: Sys_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sys_TB(
    );
    
    reg CLK;
    reg RESET;
    wire [11:0] VGA_OUT;
    wire VGA_HS;
    wire VGA_VS;
    
    System s_uut(
        .CLK(CLK),
        .RESET(RESET),
        // VGA inputs/outputs
        .VGA_OUT(VGA_OUT),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS)
    );
        
    initial begin
        CLK = 0;
        forever #5 CLK = ~CLK;
    end
    
    initial begin
        RESET = 1;
        #50 RESET = 0;
    end
    
endmodule
