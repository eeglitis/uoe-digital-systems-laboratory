`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis
// 
// Create Date: 07.03.2017
// Design Name: ROM (instruction memory)
// Module Name: ROM
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The ROM module, containing all instructions and memory addresses
//              required for the program execution.
// 
// Dependencies: None
// 
// Revision: 1.01 - readmemh changed to readmemb
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ROM(
	//standard signals
	input              CLK,
	//BUS signals
	output reg [7:0]   DATA,
	input      [7:0]   ADDR
);

parameter RAMAddrWidth = 8;

//Memory
reg [7:0] ROM [2**RAMAddrWidth-1:0];

// Load program
initial $readmemb("ROM.txt", ROM);

//single port ram
always@(posedge CLK)
	DATA <= ROM[ADDR];

endmodule
