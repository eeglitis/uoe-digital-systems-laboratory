`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: The University of Edinburgh     
// Engineer: Justas Lukosiunas
// 
// Create Date: 17.02.2017 17:25:30
// Design Name: Mouse_interface
// Module Name: LED_Peripheral
// Project Name: Digital Systems Laboratory
// Target Devices: BASYS 3
// Tool Versions: 
// Description: This module ties the 8 rightmost LEDs on the BASYS board
//              to the DATA_BUS, during the specified LED driver data adress -
//              0xC0.
// Dependencies: None
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module LED_Peripheral(
    inout  [7:0]   BUS_DATA,
    input  [7:0]   BUS_ADDR,
    input          BUS_WE,
    input          CLK,
    input          RESET,
    output [7:0]   LED_OUT
    );
    
    
        localparam
        LED_ADDR = 8'hC0;
        
        //register keepign values for the seven segmenty displays
        reg [7:0] CurrLedValue, NextLedValue;
        
        // setting up the data bus for only reading
        reg write_enable = 1'b0;
        assign BUS_DATA  = write_enable ? 8'h00 : 8'hZZ;
    
        //Sequential part of the state machine
        always@(posedge CLK)begin 
            if(RESET)
                begin        
                    CurrLedValue <= 0;
                end    
            else
                begin          
                    CurrLedValue <= NextLedValue;
                end
        end
        
        //Combinatorial
        always@*
        begin
            //default assignment to keep valeus the same
            NextLedValue = CurrLedValue;
            
            if(BUS_ADDR == LED_ADDR)
                NextLedValue = BUS_DATA;
                
        end    
        
        assign LED_OUT = CurrLedValue;
               
endmodule
