`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: The University of Edinburgh
// Engineer: Justas Lukosiunas
// 
// Create Date: 17.02.2017 17:25:30
// Design Name: Mouse_interface
// Module Name: Seven_seg_peripheral
// Project Name: Digital Systems Laboratory
// Target Devices: BASYS 3
// Tool Versions: 
// Description: This module wraps up previously implemented 7 segment
//              display module into the peripheral interface - it now
//              sends data to BUS_DATA when the BUS_ADDR reaches the assigned
//              address - 0xD0 (for the 2 displays on the left) and 0xD1 (for the
//              two displays on the right). 
// 
// Dependencies: Generic_counter2, Multiplexer_4way, Seg7_display
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Seven_seg_peripheral(
    inout  [7:0]   BUS_DATA,
    input  [7:0]   BUS_ADDR,
    input          BUS_WE,
    input          CLK,
    input          RESET,
    output [3:0]   SEG_SELECT,
    output [7:0]   DEC_OUT 
);
    
    // defining the allocated addresses for the 7 segment
    //   display driver.
    localparam
    ADDRX = 8'hD0,
    ADDRY = 8'hD1;


    wire [3:0] MuxOut;         
    wire Bit17TriggOut;
    wire Bit4_1TriggOut;  
    wire [1:0] StrobeCount;
        
    //INSTANTIATIONS   
    //The 17bit counter
    Generic_counter2 # (.COUNTER_WIDTH(17),
                       .COUNTER_MAX(99999)
                       )
                       Bit17Counter (
                       .CLK(CLK),
                       .RESET(1'b0),
                       .ENABLE(1'b1),
                       .TRIGGER_OUT(Bit17TriggOut)
                       );  
  
    //2bit counter 
    Generic_counter2 # (.COUNTER_WIDTH(2),
                       .COUNTER_MAX(3)
                       )
                       Bit2Counter (
                       .CLK(CLK),
                       .RESET(RESET),
                       .ENABLE(Bit17TriggOut),
                       .COUNT(StrobeCount)
                       );
                
    reg [7:0] Curr_X_COORD, Next_X_COORD; 
    reg [7:0] Curr_Y_COORD, Next_Y_COORD; 
    
    //configuring the data bus for reading
    reg write_enable = 1'b0;
    assign BUS_DATA  = write_enable ? 8'h00 : 8'hZZ;
    
       
    //Sequential
    always@(posedge CLK) begin
       if(RESET) begin
           Curr_X_COORD <= 0;
           Curr_Y_COORD <= 0;
       end else begin
           Curr_X_COORD <= Next_X_COORD;
           Curr_Y_COORD <= Next_Y_COORD;
       end
    end
       
    //Combinatorial
    always@* begin
       //default value assignments for maintaining 
       // the current coordinate values when the 
       // correct address is not reached
       Next_X_COORD = Curr_X_COORD;
       Next_Y_COORD = Curr_Y_COORD;
       
       //Once the address bus is set to correct address,
       // setting the new coordinates from the data bus
       if (BUS_ADDR == ADDRX)
            Next_X_COORD = BUS_DATA;  
       else if (BUS_ADDR == ADDRY) 
            Next_Y_COORD = BUS_DATA; 
    end
    
    

    
    //Instatiate the Multiplexer
    Multiplexer_4way Mux4(
                .CONTROL(StrobeCount),
                .IN0(Curr_Y_COORD[3:0]), //y coordinate
                .IN1(Curr_Y_COORD[7:4]), //y coordinate
                .IN2(Curr_X_COORD[3:0]), //x coordinate
                .IN3(Curr_X_COORD[7:4]), //x coordinate
                .OUT(MuxOut)
                );
                
    
    //Instantiating 7seg display low level driver
    Seg7_display seg_mod7(
                .SEG_SELECT_IN(StrobeCount),
                .BIN_IN(MuxOut),
                .DOT_IN(1'b0),
                .SEG_SELECT_OUT(SEG_SELECT),
                .HEX_OUT(DEC_OUT)
                );
    
    
endmodule
