`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: The University of Edinburgh
// Engineer: Justas Lukosiunas
// 
// Create Date: 10/26/2015 11:46:41 AM
// Design Name: 
// Module Name: Generic_counter
// Project Name: World of Linked State Machines
// Target Devices: xc7a35tcpg236-1
// Tool Versions: Basys3
// Description: Generic counter module that has variable bit width and maximum count value
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Generic_counter2(
        CLK,
        RESET,
        ENABLE,
        TRIGGER_OUT,
        COUNT
);

    parameter COUNTER_WIDTH = 4;
    parameter COUNTER_MAX   = 9;

    input CLK;
    input RESET;
    input ENABLE;
    output TRIGGER_OUT;
    output [COUNTER_WIDTH-1:0] COUNT;
    
    //Declare registers that hold current count value
    //and trigger output between clock cycles
    reg [COUNTER_WIDTH-1:0] count_value;
    reg Trigger_out;

    
    
    //Synchronous logic for value of count_value
    initial 
       count_value = 0;
    always@(posedge CLK) begin
        if(RESET)
            count_value <= 0;
        else begin 
            if(ENABLE) begin
                if(count_value == COUNTER_MAX)
                    count_value <= 0;
                else
                    count_value <= count_value +1;
            end
        end
    end      
    

    //Synchronous logic for Trigger_out
    always@(posedge CLK) begin
        if(RESET)
            Trigger_out <=0;
        else begin
            if(ENABLE && (count_value == COUNTER_MAX))      
                Trigger_out <= 1;
            else
                Trigger_out <= 0;
        end
    end  
    
    //Assignment that ties the count_value and Trigger_out to
    //COUNT and TRIG_OUT respectively
    assign COUNT    = count_value;
    assign TRIGGER_OUT = Trigger_out;  
    
endmodule    
