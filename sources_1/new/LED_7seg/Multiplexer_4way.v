`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: The University of Edinburgh
// Engineer: Justas Lukosiunas
// 
// Create Date: 10/26/2015 01:19:40 AM
// Design Name: 
// Module Name: Multiplexer_4way
// Project Name: World of Linked State Machines
// Target Devices: xc7a35tcpg236-1
// Tool Versions: Basys3
// Description: 4 to 1 multiplexer, takes 4 4bit inputs, uses 2bit control signal, outputs 1 of 4.
//        
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Multiplexer_4way(
    input [1:0] CONTROL,
    input [3:0] IN0,
    input [3:0] IN1,
    input [3:0] IN2,
    input [3:0] IN3,
    output reg [3:0] OUT
);
    //whenever the Control signal or one of the inputs change 
    always@(    CONTROL     or
                IN0         or
                IN1         or
                IN2         or
                IN3         
                )
    //do the following
    //wire different inputs to the output, depending on the control signal state
    begin
        case(CONTROL)
            2'b00       : OUT <= IN0;
            2'b01       : OUT <= IN1;
            2'b10       : OUT <= IN2;
            2'b11       : OUT <= IN3;
            default     : OUT <= 4'b0000;
        endcase
    end
    
endmodule
