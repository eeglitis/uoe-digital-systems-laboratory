`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: IRsignal_generator
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module ties the right frequency from Frequency_generator with the packet state
//              to generate a required IR signal
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module IRsignal_generator(
    input CAR_FREQ,
    input [3:0] STATE_IN,
    output IRsignal
    );

    // Define the states of the packet: START, CAR_SELECT, RIGHT_ASSERT and RIGHT_DEASSERT, LEFT_ASSERT and LEFT_DEASSERT,
    // BACKWARD_ASSERT and BACKWARD_DEASSERT, FORWARD_ASSERT and FORWARD_DEASSERT
    parameter START = 4'b0000;
    parameter CAR_SELECT = 4'b0010;
    parameter RIGHT_ASSERT = 4'b0100;
    parameter RIGHT_DEASSERT = 4'b0101;
    parameter LEFT_ASSERT = 4'b0111;
    parameter LEFT_DEASSERT = 4'b1000;
    parameter BACKWARD_ASSERT = 4'b1010;
    parameter BACKWARD_DEASSERT = 4'b1011;
    parameter FORWARD_ASSERT = 4'b1101;
    parameter FORWARD_DEASSERT = 4'b1110;

    // Declare a register IRsignal_out that holds the current value
    reg IRsignal_out;

    // Combinational logic
    // If the current state corresponds to one of the BURSTS (START, CAR_SELECT, RIGHT_ASSERT, RIGHT_DEASSERT,...),
    // then IRsignal_out has a frequency for the selected car. Otherwise, IRsignal_out is ZERO.
    always@(CAR_FREQ or IRsignal_out or STATE_IN) begin
        if(STATE_IN == START || STATE_IN == CAR_SELECT || STATE_IN == RIGHT_ASSERT || STATE_IN == LEFT_ASSERT || 
           STATE_IN == BACKWARD_ASSERT || STATE_IN == FORWARD_ASSERT || STATE_IN == RIGHT_DEASSERT ||
           STATE_IN == LEFT_DEASSERT || STATE_IN == BACKWARD_DEASSERT || STATE_IN == FORWARD_DEASSERT) 
            IRsignal_out <= CAR_FREQ;
        else
            IRsignal_out <= 0;
    end
    
    // Tie the IRsignal_out with IRsignal
    assign IRsignal = IRsignal_out;
endmodule
