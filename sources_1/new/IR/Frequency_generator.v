`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Frequency_generator
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module based on a generic variable length counter gnerates 
//              the right frequency for the car.
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Frequency_generator(
    input CLK,
    input [11:0] COUNTER_MAX,
    output OUT
    );

    // Define COUNTER_WIDTH and COUNTER_MAX values in order to
    // control their variability 
    parameter COUNTER_WIDTH = 4;
//    parameter COUNTER_MAX = 9;

    // Declare registers that hold the current count value and trigger out
    // between clock cycles  
    reg [COUNTER_WIDTH-1:0] count_value;
    reg Trigger_out;
    
    // Initialize the counter to 0
    initial
        count_value = 10'd0;
    
    // Synchronous logic for value of count_value
    always@(posedge CLK)begin
        if(count_value == COUNTER_MAX)
            count_value <= 0;
        else
            count_value <= count_value + 1;
    end
    
    // Synchronous logic for Trigger_out
    // NOTE: count_value <= COUNTER_MAX/2 ensures ~50% duty cycle 
    always@(posedge CLK)begin
        if(count_value <= COUNTER_MAX/2)
            Trigger_out <= 1;
        else
            Trigger_out <= 0;
    end
    
    // Tie the Trigger_out with OUT
    assign OUT = Trigger_out;
endmodule
