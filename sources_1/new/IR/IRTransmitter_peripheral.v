`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 03/21/2017 
// Module Name: IRTransmitter_peripheral
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module generates proper pulse codes to control cars
//              remotely using BASYS3 FPGA board with a given IR Transmitter
//              peripheral module. The pulse code can be adjusted by manually
//              changing the frequency of the Frequency_generator and number of 
//              pulses in each packet controlled by Control_packet_state_generator_SM.
// 
// Dependencies: Frequency_generator
//               Send_packet_frequency_generator
//               Packet_state_generator_SM
//               Control_packet_state_generator_SM
//               IRsignal_generator
//               Car_select
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
//
//////////////////////////////////////////////////////////////////////////////////


module IRTransmitter_peripheral(
    input CLK,
    input RESET,
    input [3:0] CAR_SELECT,
    
    // BUS signals
    input [7:0] BUS_ADDR,
    input [7:0] BUS_DATA,
    
    output IR_LED
    );
    
    // IRTransmitter Base Address in the Memory Map
    parameter [7:0] IRTransmitterBaseAddr = 8'h90; 
   
   // Initial command 
    parameter [3:0] InitialCommand = 4'b0000;
    
    // COMMAND register to be loaded with constant value from data memory
    reg [3:0] COMMAND;
    
    always@(posedge CLK) begin
        if(RESET)
            COMMAND <= InitialCommand;
        else if(BUS_ADDR == IRTransmitterBaseAddr) 
            COMMAND <= BUS_DATA[3:0];
    end
    
    // Declare wires for CarFreqCounter, StartBurstSize, GapSize, CarSelectBurstSize, AssertBurstSize,
    // and DeAssertBurstSize to tie Car_select with Control_packet_state_generator_SM
    wire [11:0] CarFreqCounter;
    wire [8:0] StartBurstSize;
    wire [5:0] GapSize;
    wire [5:0] CarSelectBurstSize;
    wire [6:0] AssertBurstSize;
    wire [5:0] DeAssertBurstSize;
    
    // Declare wires for StartBurstTrig, GapTrig CarSelectBurstTrig, AssertBurstTrig and DeAssertBurstTrig
    // to tie Control_packet_state_generator_SM with Packet_state_generator_SM   
    wire StartBurstTrig;
    wire GapTrig;
    wire CarSelectBurstTrig;
    wire AssertBurstTrig;
    wire DeAssertBurstTrig;
    
    // Declare a wire for STATE to tie Packet_state_generator_SM with Control_packet_state_generator_SM and 
    // IRsignal_generator
    wire [3:0] STATE;
    
    // Declare a wire for CAR_FREQ to tie Frequency_generator with Control_packet_state_generator_SM and
    // IRsignal_generator
    wire CAR_FREQ;
    
    // Declare a wire for SEND_PACKET to tie Send_packet_frequency_generator with Packet_state_generator_SM
    wire SEND_PACKET;
    
    // Enables response for different cars
    Car_select Select (
        .CAR_SELECT(CAR_SELECT),
        .CarFreqCounter(CarFreqCounter),
        .StartBurstSize(StartBurstSize),
        .GapSize(GapSize),
        .CarSelectBurstSize(CarSelectBurstSize),
        .AssertBurstSize(AssertBurstSize),
        .DeAssertBurstSize(DeAssertBurstSize)
    );
    
    // Frequency_generator gnerates the right frequency for the car
    Frequency_generator # (.COUNTER_WIDTH(12)
                           )
                           CarFrequency (
                           .CLK(CLK),
                           .COUNTER_MAX(CarFreqCounter),
                           .OUT(CAR_FREQ)           
    );
    
    // Gives frequency of 10Hz: 100MHz/10Hz=10 000 000 -> 9 999 999 for counter value and 24 bits for width
    Send_packet_frequency_generator # (.COUNTER_WIDTH(24),
                                       .COUNTER_MAX(9999999)
                                       )
                                       SendPacketFrequency (
                                       .CLK(CLK),
                                       .SEND_PACKET(SEND_PACKET)           
    );
    
    // A simple state machine to generate the states of the packet
    Packet_state_generator_SM SM (
                    .CLK(CLK),
                    .RESET(RESET),
                    .SEND_PACKET(SEND_PACKET),
                    .StartBurstTrig(StartBurstTrig),
                    .GapTrig(GapTrig),
                    .CarSelectBurstTrig(CarSelectBurstTrig),
                    .AssertBurstTrig(AssertBurstTrig),
                    .DeAssertBurstTrig(DeAssertBurstTrig),
                    .COMMAND(COMMAND),
                    .STATE_OUT(STATE)
    );
    
    // Control_packet_state_generator_SM controlls the states of the packet - ensures a correct number of pulses
    // in each packet region
     Control_packet_state_generator_SM ControlSM (
                                         .CAR_FREQ(CAR_FREQ),
                                         .RESET(RESET),
                                         .STATE_IN(STATE),
                                         .StartBurstSize(StartBurstSize),
                                         .GapSize(GapSize),
                                         .CarSelectBurstSize(CarSelectBurstSize),
                                         .AssertBurstSize(AssertBurstSize),
                                         .DeAssertBurstSize(DeAssertBurstSize),
                                         .StartBurstTrigOut(StartBurstTrig),
                                         .GapTrigOut(GapTrig),
                                         .CarSelectBurstTrigOut(CarSelectBurstTrig),
                                         .AssertBurstTrigOut(AssertBurstTrig),
                                         .DeAssertBurstTrigOut(DeAssertBurstTrig)
     );
     
     // IRsignal_generator ties the right frequency from Frequency_generator with the packet state
     // to generate IR_LED
     IRsignal_generator IRsignal (
                .CAR_FREQ(CAR_FREQ),
                .STATE_IN(STATE),
                .IRsignal(IR_LED)
    );
     
endmodule
