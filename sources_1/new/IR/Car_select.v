`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Car_select
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module provides the required parameters for a selected car
//              depending on which switch is turned on.
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Car_select(
    input [3:0] CAR_SELECT,
    output reg [11:0] CarFreqCounter,
    output reg [8:0] StartBurstSize,
    output reg [5:0] GapSize,
    output reg [5:0] CarSelectBurstSize,
    output reg [6:0] AssertBurstSize,
    output reg [5:0] DeAssertBurstSize
    );
    
    // Define the switches state
    parameter RED = 4'b0001;
    parameter BLUE = 4'b0010;
    parameter GREEN = 4'b0100;
    parameter YELLOW = 4'b1000;
    
    // YELLOW   
    // Gives frequency of 40kHz: 100MHz/40kHz=2500 -> 2499 for counter value and 12 bits for width
    // BLUE & RED  
    // Gives frequency of ~35 997Hz: 100MHz/36kHz~2778 -> 2777 for counter value and 12 bits for width
    // GREEN
    // Gives frequency of ~37 495Hz: 100MHz/37.5kHz=2667 -> 2667 for counter value and 12 bits for width
    
    //                      Start   Gap     CarSelect   Assert  DeAssert
    // Blue-coded cars      191     25      47          47      22
    // Yellow-coded cars    88      40      22          44      22
    // Green-coded cars     88      40      44          44      22
    // Red-coded cars       192     24      24          48      24
    // NOTE: for the correct number of pulses in each packet region, subtract 1 from the BurstSize or GapSize
    
    // RED-coded cars
    parameter CarFreqCounterRED = 2777;
    parameter StartBurstSizeRED = 191;
    parameter GapSizeRED = 23;
    parameter CarSelectBurstSizeRED = 23;
    parameter AssertBurstSizeRED = 47;
    parameter DeAssertBurstSizeRED = 23;
    
    // BLUE-coded cars
    parameter CarFreqCounterBLUE = 2777;
    parameter StartBurstSizeBLUE = 190;
    parameter GapSizeBLUE = 24;
    parameter CarSelectBurstSizeBLUE = 46;
    parameter AssertBurstSizeBLUE = 46;
    parameter DeAssertBurstSizeBLUE = 21;

    // GREEN-coded cars
    parameter CarFreqCounterGREEN = 2667;
    parameter StartBurstSizeGREEN = 87;
    parameter GapSizeGREEN = 39;
    parameter CarSelectBurstSizeGREEN = 43;
    parameter AssertBurstSizeGREEN = 43;
    parameter DeAssertBurstSizeGREEN = 21;
    
    // YELLOW-coded cars
    parameter CarFreqCounterYELLOW = 2499;
    parameter StartBurstSizeYELLOW = 87;
    parameter GapSizeYELLOW = 39;
    parameter CarSelectBurstSizeYELLOW = 21;
    parameter AssertBurstSizeYELLOW = 43;
    parameter DeAssertBurstSizeYELLOW = 21;
    
    // This block selects the correct value for CarFreqCounter, StartBurstSize, GapSize,
    // CarSelectBurstSize, AssertBurstSize and DeAssertBurstSize depending on the position
    // of the switches (CAR_SELECT input). If none of the switches is ON, then all the
    // registers are equal to 0.
    always@(CAR_SELECT or CarFreqCounter or StartBurstSize or GapSize or CarSelectBurstSize or AssertBurstSize or DeAssertBurstSize) begin
        if(CAR_SELECT == RED) begin
            CarFreqCounter <= CarFreqCounterRED;
            StartBurstSize <= StartBurstSizeRED;
            GapSize <= GapSizeRED;
            CarSelectBurstSize <= CarSelectBurstSizeRED;
            AssertBurstSize <= AssertBurstSizeRED;
            DeAssertBurstSize <= DeAssertBurstSizeRED;
        end
        else if(CAR_SELECT == BLUE) begin
            CarFreqCounter <= CarFreqCounterBLUE;
            StartBurstSize <= StartBurstSizeBLUE;
            GapSize <= GapSizeBLUE;
            CarSelectBurstSize <= CarSelectBurstSizeBLUE;
            AssertBurstSize <= AssertBurstSizeBLUE;
            DeAssertBurstSize <= DeAssertBurstSizeBLUE;
        end
        else if(CAR_SELECT == GREEN) begin
            CarFreqCounter <= CarFreqCounterGREEN;
            StartBurstSize <= StartBurstSizeGREEN;
            GapSize <= GapSizeGREEN;
            CarSelectBurstSize <= CarSelectBurstSizeGREEN;
            AssertBurstSize <= AssertBurstSizeGREEN;
            DeAssertBurstSize <= DeAssertBurstSizeGREEN;
        end
        else if(CAR_SELECT == YELLOW) begin
            CarFreqCounter <= CarFreqCounterYELLOW;
            StartBurstSize <= StartBurstSizeYELLOW;
            GapSize <= GapSizeYELLOW;
            CarSelectBurstSize <= CarSelectBurstSizeYELLOW;
            AssertBurstSize <= AssertBurstSizeYELLOW;
            DeAssertBurstSize <= DeAssertBurstSizeYELLOW;
        end
        else begin
            CarFreqCounter <= 0;
            StartBurstSize <= 0;
            GapSize <= 0;
            CarSelectBurstSize <= 0;
            AssertBurstSize <= 0;
            DeAssertBurstSize <= 0;
        end
    end 
endmodule
