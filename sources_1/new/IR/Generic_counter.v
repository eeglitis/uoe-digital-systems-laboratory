`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Generic_counter
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module based on a generic variable length counter is used to 
//              count the number of pulses in each packet region. The counter is
//              enabled by the state of the packet and disabled when TRIG_OUT is 1
//              which means that a required number of pulses has been achieved.
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Generic_counter(
    input CLK,
    input RESET,
    input [3:0] ENABLE_IN,
    // COUNTER_MAX has 9 bits to allow to use the same module for different 
    // length counters(e.g. StartBurst, Gap, CarSelectBurst and etc.) 
    input [8:0] COUNTER_MAX,        
    output TRIG_OUT
    );
    
    // Define COUNTER_WIDTH and COUNTER_MAX values in order to
    // control its variability  
    parameter COUNTER_WIDTH = 4;
//    parameter COUNTER_MAX = 9;
    
    // Define STATE_IN value in order to control
    // its variability
    parameter STATE_IN = 4'b0000;  
    
    // Declare registers that hold the current count value and trigger out
    // between clock cycles
    reg [COUNTER_WIDTH-1:0] count_value;
    reg Trigger_out;
    
    // Initialize the counter to 0
    initial
        count_value = 10'd0;
    
    // Synchronous logic for value of count_value
    always@(negedge CLK)begin
        if(RESET)
            count_value <= 0;
        else begin
            if(ENABLE_IN == STATE_IN)begin
                if(count_value == COUNTER_MAX)
                    count_value <= 0;
                else
                    count_value <= count_value + 1;
            end
        end
    end
    
    // Synchronous logic for Trigger_out
    always@(negedge CLK)begin
        if(RESET)
            Trigger_out <= 0;
        else begin
            if((ENABLE_IN == STATE_IN) && (count_value == COUNTER_MAX))
                Trigger_out <= 1;
            else
                Trigger_out <= 0;
        end
    end

    // Tie the Trigger_out with TRIG_OUT
    assign TRIG_OUT = Trigger_out;
endmodule
