`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Control_packet_state_generator_SM
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module controlls the states of the packet - ensures a correct number of pulses
//              in each packet region. This is done by sending the inpulse to Packet_state_generator_SM 
//              when a correct burst or gap size is achieved. Therefore, the module uses 5 Generic_counters:
//              one for each packet region (Start, Gap, CarSelect, Assert or DeAssert).
// 
// Dependencies: Generic_counter
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Control_packet_state_generator_SM(
    input CAR_FREQ,
    input RESET,
    input [3:0] STATE_IN,
    input [8:0] StartBurstSize,
    input [5:0] GapSize,
    input [5:0] CarSelectBurstSize,
    input [6:0] AssertBurstSize,
    input [5:0] DeAssertBurstSize,
    output StartBurstTrigOut,
    output GapTrigOut,
    output CarSelectBurstTrigOut,
    output AssertBurstTrigOut,
    output DeAssertBurstTrigOut
    );
    
    // Declare a register for STATE that hold a current STATE value for the same size
    // GAP(==GAP0=GAP1=GAP2=GAP3=GAP4), ASSERT(=RIGHT_ASSERT=LEFT_ASSERT=BACKWARD_ASSERT=FORWARD_ASSERT) 
    // and DEASSERT(=RIGHT_DEASSERT=LEFT_DEASSERT=BACKWARD_DEASSERT=FORWARD_DEASSERT) states 
    reg [3:0] STATE;
    
    // Declare wires for StartBurstTrig, GapTrig CarSelectBurstTrig, AssertBurstTrig and DeAssertBurstTrig
    // to send a pulse to the Packet_state_generator_SM when a correct burst or gap size is achieved. 
    wire StartBurstTrig;
    wire GapTrig;
    wire CarSelectBurstTrig;
    wire AssertBurstTrig;
    wire DeAssertBurstTrig;
    
    // Define the states of the packet: START, GAP(=GAP0=GAP1=GAP2=GAP3=GAP4), CAR_SELECT, RIGHT_ASSERT and RIGHT_DEASSERT,
    // LEFT_ASSERT and LEFT_DEASSERT, BACKWARD_ASSERT and BACKWARD_DEASSERT, FORWARD_ASSERT and FORWARD_DEASSERT
    parameter START = 4'b0000;
    parameter GAP = 4'b0001;
    parameter CAR_SELECT = 4'b0010;
    parameter GAP0 = 4'b0011;
    parameter RIGHT_ASSERT = 4'b0100;
    parameter RIGHT_DEASSERT = 4'b0101;
    parameter GAP1 = 4'b0110;
    parameter LEFT_ASSERT = 4'b0111;
    parameter LEFT_DEASSERT = 4'b1000;
    parameter GAP2 = 4'b1001;
    parameter BACKWARD_ASSERT = 4'b1010;
    parameter BACKWARD_DEASSERT = 4'b1011;
    parameter GAP3 = 4'b1100;
    parameter FORWARD_ASSERT = 4'b1101;
    parameter FORWARD_DEASSERT = 4'b1110;
    parameter GAP4 = 4'b1111;
    
    // Define RIGHT_ASSERT=LEFT_ASSERT=BACKWARD_ASSERT=FORWARD_ASSERT or GAP0=GAP1=GAP2=GAP3=GAP4 or
    // RIGHT_DEASSERT=LEFT_DEASSERT=BACKWARD_DEASSERT=FORWARD_DEASSERT as a single state because
    // all of them have the same BurstSize or GapSize and, in this case, only a number of pulses in each
    // packet region matters.
    always@(STATE or STATE_IN) begin
        if(STATE_IN == RIGHT_ASSERT || STATE_IN == LEFT_ASSERT || STATE_IN == BACKWARD_ASSERT || STATE_IN == FORWARD_ASSERT)
            STATE <= RIGHT_ASSERT;
        else if(STATE_IN == GAP0 || STATE_IN == GAP || STATE_IN == GAP1 || STATE_IN == GAP2 || STATE_IN == GAP3)
            STATE <= GAP;
        else if(STATE_IN == RIGHT_DEASSERT || STATE_IN == LEFT_DEASSERT || STATE_IN == BACKWARD_DEASSERT || STATE_IN == FORWARD_DEASSERT)
            STATE <= RIGHT_DEASSERT;
        else
            STATE <= START;
    end
 
    // Generic_counter counts the number of pulses in a certain packet region and a TRIG_OUT signal
    // sends the inpulse to Packet_state_generator_SM when a correct burst or gap size is achieved
    
    // START BURST
    Generic_counter # (.COUNTER_WIDTH(8),
                       .STATE_IN(START)
                       )
                       StartCounter (
                       .CLK(CAR_FREQ),
                       .RESET(RESET),
                       .ENABLE_IN(STATE_IN),
                       .COUNTER_MAX(StartBurstSize),
                       .TRIG_OUT(StartBurstTrig)           
    );
    
    
    // GAP
    Generic_counter # (.COUNTER_WIDTH(6),
                       .STATE_IN(GAP)
                       )
                       GapCounter (
                       .CLK(CAR_FREQ),
                       .RESET(RESET),
                       .ENABLE_IN(STATE),
                       .COUNTER_MAX(GapSize),
                       .TRIG_OUT(GapTrig)           
    );
    
    // CAR SELECT BURST
    Generic_counter # (.COUNTER_WIDTH(6),
                       .STATE_IN(CAR_SELECT)
                       )
                       CarSelectCounter (
                       .CLK(CAR_FREQ),
                       .RESET(RESET),
                       .ENABLE_IN(STATE_IN),
                       .COUNTER_MAX(CarSelectBurstSize),
                       .TRIG_OUT(CarSelectBurstTrig)           
    );
    
    // ASSERT BURST
    Generic_counter # (.COUNTER_WIDTH(6),
                       .STATE_IN(RIGHT_ASSERT)
                       )
                       AssertCounter (
                       .CLK(CAR_FREQ),
                       .RESET(RESET),
                       .ENABLE_IN(STATE),
                       .COUNTER_MAX(AssertBurstSize),
                       .TRIG_OUT(AssertBurstTrig)           
    );
    
    // DEASSERT BURST
    Generic_counter # (.COUNTER_WIDTH(5),
                       .STATE_IN(RIGHT_DEASSERT)
                       )
                       DeAssertCounter (
                       .CLK(CAR_FREQ),
                       .RESET(RESET),
                       .ENABLE_IN(STATE),
                       .COUNTER_MAX(DeAssertBurstSize),
                       .TRIG_OUT(DeAssertBurstTrig)           
    );
    
    // Tie the StartBurstTrigOut with StartBurstTrig
    // Tie the GapTrigOut with GapTrig
    // Tie the CarSelectBurstTrigOut with CarSelectBurstTrig
    // Tie the AssertBurstTrigOut with AssertBurstTrig
    // Tie the DeAssertBurstTrigOut with DeAssertBurstTrig
    assign StartBurstTrigOut = StartBurstTrig;
    assign GapTrigOut = GapTrig;  
    assign CarSelectBurstTrigOut = CarSelectBurstTrig;
    assign AssertBurstTrigOut = AssertBurstTrig;
    assign DeAssertBurstTrigOut = DeAssertBurstTrig;  
endmodule
