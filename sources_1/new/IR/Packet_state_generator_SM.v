`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Packet_state_generator_SM
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module generates the states of the packet. COMMAND input consists 
//              of four bits: bit 0 to assert or de-assert the right direction, bit 1 
//              to assert or de-assert the left direction, bit 2 to assert or de-assert 
//              the backward direction, and bit 3 to assert or de-assert the forward 
//              direction. The state machine is enabled by setting SEND_PACKET to '1' for 
//              one clock cycle, which will result in the generation of one single packet
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Packet_state_generator_SM(
    input CLK,
    input RESET,                         
    input SEND_PACKET,
    input StartBurstTrig,
    input GapTrig,
    input CarSelectBurstTrig,
    input AssertBurstTrig,
    input DeAssertBurstTrig,
    input [3:0] COMMAND,
    output [3:0] STATE_OUT
    );
    
    // Define the states of the packet: START, GAP(=GAP0=GAP1=GAP2=GAP3=GAP4), CAR_SELECT, RIGHT_ASSERT and RIGHT_DEASSERT,
    // LEFT_ASSERT and LEFT_DEASSERT, BACKWARD_ASSERT and BACKWARD_DEASSERT, FORWARD_ASSERT and FORWARD_DEASSERT
    parameter START = 4'b0000;
    parameter GAP = 4'b0001;
    parameter CAR_SELECT = 4'b0010;
    parameter GAP0 = 4'b0011;
    parameter RIGHT_ASSERT = 4'b0100;
    parameter RIGHT_DEASSERT = 4'b0101;
    parameter GAP1 = 4'b0110;
    parameter LEFT_ASSERT = 4'b0111;
    parameter LEFT_DEASSERT = 4'b1000;
    parameter GAP2 = 4'b1001;
    parameter BACKWARD_ASSERT = 4'b1010;
    parameter BACKWARD_DEASSERT = 4'b1011;
    parameter GAP3 = 4'b1100;
    parameter FORWARD_ASSERT = 4'b1101;
    parameter FORWARD_DEASSERT = 4'b1110;
    parameter GAP4 = 4'b1111;
    
    // Declare registers for both the current state and the next state
    reg [3:0] Curr_state;
    reg [3:0] Next_state;
    
    // Sequential Logic
    always@(posedge CLK) begin
        // This is a synchronous RESET
        // The State is enabled to the GAP4 which gives a ZERO value for IR_LED signal
        if(RESET)
            Curr_state <= GAP4;
        // This is a synchronous SEND_PACKET
        // The State Machine is enabled by setting SEND_PACKET to '1' for one clock cycle
        else if(SEND_PACKET)
            Curr_state <= START; 
        else
            // For normal operation, on the rising edge of the CAR_FREQ,
            // the current state gets to its next state. It is held at
            // this value until the next rising edge of the CAR_FREQ.
            Curr_state <= Next_state;
    end
    
        initial
            Curr_state <= GAP4;
    
    // Combinational Logic
    // The state machine progresses to the next state only when a correct 
    // burst or gap size is achieved. This is done by StartBurstTrig, GapTrig,
    // CarSelectBurstTrig, AssertBurstTrig and DeAssertBurstTrig received from
    // Control_packet_state_generator_SM. COMMAND input chooses the next state between 
    // RIGHT_ASSERT/DEASSERT, LEFT_ASSERT/DEASSERT, BACKWARD_ASSERT/DEASSERT and FORWARD_ASSERT/DEASSERT
    always@(Curr_state or COMMAND or StartBurstTrig or GapTrig or CarSelectBurstTrig or AssertBurstTrig or DeAssertBurstTrig) begin
        case(Curr_state)
        
            START : begin
                if(StartBurstTrig)
                    Next_state <= GAP;
                else
                    Next_state <= Curr_state;
            end
            
            GAP : begin
                if(GapTrig)
                    Next_state <= CAR_SELECT;
                else
                    Next_state <= Curr_state;
            end
                
            CAR_SELECT : begin
                if(CarSelectBurstTrig)
                    Next_state <= GAP0;
                else
                    Next_state <= Curr_state;
            end
                
            GAP0 : begin
                if(GapTrig) begin
                    if(COMMAND[0])                  
                        Next_state <= RIGHT_ASSERT;
                    else
                        Next_state <= RIGHT_DEASSERT;
                end
                else
                    Next_state <= Curr_state;
            end
            
            RIGHT_ASSERT : begin
                if(AssertBurstTrig)
                    Next_state <= GAP1;
                else
                    Next_state <= Curr_state;
            end   
            
            RIGHT_DEASSERT : begin
                if(DeAssertBurstTrig)
                    Next_state <= GAP1;
                else
                    Next_state <= Curr_state;
            end  
    
            GAP1 : begin
                if(GapTrig) begin
                    if(COMMAND[1])                  
                        Next_state <= LEFT_ASSERT;
                    else
                        Next_state <= LEFT_DEASSERT;
                end
                else
                    Next_state <= Curr_state;
            end
            
            LEFT_ASSERT : begin
                if(AssertBurstTrig)
                    Next_state <= GAP2;
                else
                    Next_state <= Curr_state;
            end 
            
            LEFT_DEASSERT : begin
                if(DeAssertBurstTrig)
                    Next_state <= GAP2;
                else
                    Next_state <= Curr_state;
            end 
    
            GAP2 : begin
                if(GapTrig) begin
                    if(COMMAND[2])                  
                        Next_state <= BACKWARD_ASSERT;
                    else
                        Next_state <= BACKWARD_DEASSERT;
                end
                else
                    Next_state <= Curr_state;
            end
            
            BACKWARD_ASSERT : begin
                if(AssertBurstTrig)
                    Next_state <= GAP3;
                else
                    Next_state <= Curr_state;
            end 
            
            BACKWARD_DEASSERT : begin
                if(DeAssertBurstTrig)
                    Next_state <= GAP3;
                else
                    Next_state <= Curr_state;
            end 
            
            GAP3 : begin
                if(GapTrig) begin
                    if(COMMAND[3])                  
                        Next_state <= FORWARD_ASSERT;
                    else
                        Next_state <= FORWARD_DEASSERT;
                end
                else
                    Next_state <= Curr_state;
            end
            
            FORWARD_ASSERT : begin
                if(AssertBurstTrig)
                    Next_state <= GAP4;
                else
                    Next_state <= Curr_state;
            end 
            
            FORWARD_DEASSERT : begin
                if(DeAssertBurstTrig)
                    Next_state <= GAP4;
                else
                    Next_state <= Curr_state;
            end
                
            GAP4 : 
                Next_state <= Curr_state;
                
            default : 
                Next_state <= START;
        endcase
    end
    
    // Tie the current state with the output
    assign STATE_OUT = Curr_state;                 
endmodule
