`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Deividas Krukauskas
// 
// Create Date: 02/28/2017 
// Module Name: Send_packet_frequency_generator
// Project Name: IR Transmitter
// Target Devices: BASYS3 FPGA board xc7a35tcpg236-1
// Description: The module based on a generic variable length counter is used to 
//              give a frequency of 10Hz.
// 
// Dependencies: N/A
// 
// Additional Comments: For more explicit explanation, see the block comments above each section
// 
//////////////////////////////////////////////////////////////////////////////////


module Send_packet_frequency_generator(
    input CLK,
    output SEND_PACKET
    );
    
    // Define COUNTER_WIDTH and COUNTER_MAX values in order to
    // control the variability of each module
    parameter COUNTER_WIDTH = 4;
    parameter COUNTER_MAX = 9;
    
    // Declare registers that hold the current count value and trigger out
    // between clock cycles
    reg [COUNTER_WIDTH-1:0] count_value;
    reg Trigger_out;
    
    
    // Initialize the counter to 0 and Trigger_out to 1
    initial begin
        count_value = 10'd0;
        Trigger_out = 1;
    end
    
    // Synchronous logic for value of count_value
    always@(posedge CLK)begin
        if(count_value == COUNTER_MAX)
            count_value <= 0;
        else
            count_value <= count_value + 1;
    end
    
    // Synchronous logic for Trigger_out
    always@(posedge CLK)begin
        if(count_value == COUNTER_MAX)
            Trigger_out <= 1;
        else
            Trigger_out <= 0;
    end
    
    // Tie the Trigger_out with SEND_PACKET
    assign SEND_PACKET = Trigger_out;
endmodule
