`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis
// 
// Create Date: 07.03.2017
// Design Name: Frame Buffer
// Module Name: Frame_Buffer
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The VGA frame buffer module, implementing the memory for the values
//              indicating each pixel's color (foreground or background).
// 
// Dependencies: None
// 
// Revision: 1.1 - memory no longer initialized through file, only externally
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module VGA_Frame_Buffer(
    // Port A - Read/Write
    input           A_CLK,
    input           A_WE,       // Write Enable
    input [14:0]    A_ADDR,     // 8 + 7 bits = 15 bits hence [14:0]
    input           A_DATA_IN,  // Pixel Data In
    output reg      A_DATA_OUT,
    // Port B - Read Only
    input           B_CLK,
    input [14:0]    B_ADDR,     // Pixel Data Out
    output reg      B_DATA
    );
    
    // A 256 x 128 1-bit memory to hold frame data
    // The LSBs of the address correspond to the X axis, and the MSBs to the Y axis
    reg [0:0] Mem [2**15-1:0];

    // Port A - Read/Write e.g. to be used by microprocessor
    always@(posedge A_CLK) begin
        if(A_WE)
            Mem[A_ADDR] <= A_DATA_IN;
        A_DATA_OUT <= Mem[A_ADDR];
    end
    
    // Port B - Read Only e.g. to be read from the VGA signal generator module for display
    always@(posedge B_CLK)
        B_DATA <= Mem[B_ADDR];
        
endmodule
