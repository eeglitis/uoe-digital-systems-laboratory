`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis
// 
// Create Date: 07.03.2017
// Design Name: VGA Signal Generator
// Module Name: VGA_Sig_Gen
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The VGA signal generator module of the project.
//              This module implements the pulse scan counters,
//              sets the values of pixel address and sync pulses,
//              and specifies the valid timing for colour values.
//              Finally, it specifies the colour of each bit
//              depending on its memory value.
// 
// Dependencies: None
// 
// Revision: 1.00 - Published version
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module VGA_Sig_Gen(
    input           CLK,
    input           RESET,
    //Colour Configuration Interface
    input [15:0]    CONFIG_COLOURS,
    // Frame Buffer (Dual Port memory) Interface
    input           VGA_DATA,
    output          DPR_CLK,
    output [14:0]   VGA_ADDR,
    //VGA Port Interface
    output reg      VGA_HS = 0,
    output reg      VGA_VS = 0,
    output [7:0]    VGA_COLOUR
);

// Quarter the clock from 100MHz to 25MHz to drive the VGA display
reg VGA_CLK;
reg halving;

always@(posedge CLK) begin
    if(RESET) begin
        VGA_CLK <= 0;
        halving <= 0;
    end
    else begin
        if (halving == 0)
            halving <= 1;
        else begin
            VGA_CLK <= ~VGA_CLK; 
            halving <= 0;
        end
    end
end

/*
Define VGA signal parameters e.g. Horizontal and Vertical display time,
pulse widths, front and back porch widths etc. 
*/

parameter HTs       = 800;  // Total Horizontal Sync Pulse Time
parameter HTpw      = 96;   // Horizontal Pulse Width Time
parameter HTbp      = 48;   // Horizontal Back Porch Time
parameter HTDisp    = 640;  // Horizontal Display Time
parameter HTfp      = 16;   // Horizontal Front Porch Time
parameter VTs       = 521;  // Total Vertical Sync Pulse Time
parameter VTpw      = 2;    // Vertical Pulse Width Time
parameter VTbp      = 29;   // Vertical Back Porch Time
parameter VTDisp    = 480;  // Vertical Display Time
parameter VTfp      = 10;   // Vertical Front Porch Time

// Define Horizontal and Vertical Counters (and their intermediate registers)
reg [9:0] Hcounter = 0, Vcounter = 0;
reg [9:0] Hcounter_nxt, Vcounter_nxt;

// Define pixel addresses derived from counters, generating the VGA signal
// Values limited only to the visible display pixel count
reg [9:0] Hpixel, Vpixel;

// Define intermediate registers for holding next sync values
reg VGA_HS_nxt, VGA_VS_nxt;

// Define intermediate color registers
// showing CONFIG_COLORS only within visible display range, otherwise 0
reg [15:0] COLOUR_BUF = 0;
reg [15:0] COLOUR_BUF_nxt;

/*
Create a process that assigns the proper horizontal
and vertical counter values for raster scan of the display. 
*/

// Sequential logic
always@(posedge VGA_CLK) begin
    if (RESET) begin
        Hcounter <= 0;
        Vcounter <= 0;
    end
    else begin
        Hcounter <= Hcounter_nxt;
        Vcounter <= Vcounter_nxt;
    end
end

// Combinatorial logic for incrementing counters
// Values limited to the range 0..HTs/0..VTs
always@(Hcounter or Vcounter) begin
    Hcounter_nxt = (Hcounter+1) % HTs;
    if (Hcounter == HTs-1)
        Vcounter_nxt = (Vcounter+1) % VTs;
    else
        Vcounter_nxt = Vcounter;
end

// Combinatorial logic for pixel addresses
// Increment pixel address only within visible display range,
// else set it to 0
always@(Hcounter or Vcounter) begin        
    if ((Hcounter >= HTpw+HTbp) && (Hcounter < HTpw+HTbp+HTDisp))
        Hpixel = Hcounter-HTpw-HTbp;
    else
        Hpixel = 0;
    if ((Vcounter >= VTpw+VTbp) && (Vcounter < VTpw+VTbp+VTDisp))
        Vpixel = Vcounter-VTpw-VTbp;
    else
        Vpixel = 0;
end

/*
Need to create the address of the next pixel.
Concatenate and tie the look ahead address to the frame buffer address.
*/
assign DPR_CLK = VGA_CLK;
assign VGA_ADDR = {Vpixel[8:2], Hpixel[9:2]};

/*
Create a process that generates the horizontal and vertical synchronisation signals,
as well as the pixel colour information, using HCounter and VCounter.
Do not forget to use CONFIG_COLOURS input to display the right foreground and background colours.
*/

// Sequential logic
always@(posedge VGA_CLK) begin
    if (RESET) begin
        VGA_HS <= 0;
        VGA_VS <= 0;
        COLOUR_BUF <= 0;
    end
    else begin
        VGA_HS <= VGA_HS_nxt;
        VGA_VS <= VGA_VS_nxt;
        COLOUR_BUF <= COLOUR_BUF_nxt;
    end
end

// Combinatorial logic for HS/VS control
// Assign syncs to 0 only if within pulse width interval, else 1
always@(Hcounter or Vcounter) begin
    if (Hcounter >= 0 && Hcounter < HTpw)
        VGA_HS_nxt = 0;
    else
        VGA_HS_nxt = 1;
    if (Vcounter >= 0 && Vcounter < VTpw)
        VGA_VS_nxt = 0;
    else
        VGA_VS_nxt = 1;
end

// Combinatorial logic for color control
// Only set output color if within range of display time, else 0
always@(Hcounter or Vcounter or CONFIG_COLOURS) begin
    if (Hcounter >= HTpw+HTbp && Hcounter < HTpw+HTbp+HTDisp && Vcounter >= VTpw+VTbp && Vcounter < VTpw+VTbp+VTDisp)
        COLOUR_BUF_nxt = CONFIG_COLOURS;
    else
        COLOUR_BUF_nxt = 0;
end

/*
Finally, tie the output of the frame buffer to the colour output VGA_COLOUR. 
*/
// Depending on VGA_DATA, use either foreground or background color values
assign VGA_COLOUR = (VGA_DATA) ? COLOUR_BUF[15:8] : COLOUR_BUF[7:0];

endmodule
