`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis
// 
// Create Date: 07.03.2017
// Design Name: VGA-to-Microprocessor Interface
// Module Name: VGA_Interface
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The VGA interface module, providing the access point from the
//              data and address buses to the VGA configuration.
//              Implements the protocol for correct data receival and transfer.
// 
// Dependencies: VGA_Sig_Gen, VGA_Frame_Buffer
// 
// Revision: 1.1 - Color flipping counter functionality moved to software.
//                 Added a protocol for bus input handling.
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module VGA_Interface(
    input   CLK,            // built-in FPGA clock
    input   RESET,          // button T17 on the FPGA
    input   [7:0] BUS_DATA,
    input   [7:0] BUS_ADDR,
    input         BUS_WE,
    output  [11:0] VGA_OUT, // VGA output bits
    output  VGA_HS,         // horizontal sync output bit
    output  VGA_VS          // vertical sync output bit
    );
        
    // Specify relevant addresses as parameters for ease of use
    parameter B0 = 8'b10110000;
    parameter B1 = 8'b10110001;
    parameter B2 = 8'b10110010;
    
    // Initial input/output signals
    // Frame buffer
    reg         A_WE;               // Write enable signal
    reg         A_DATA_IN;          // Pixel data bit (foreground/background)
    reg [14:0]  A_ADDR;             // Address to read/write pixel data
    wire        FB_A_DATA;          // A_DATA_OUT from frame buffer
    // Signal generator
    wire [15:0] CONFIG_COLOURS;     // background RGB + foreground RGB values
    wire [7:0]  VGA_COLOUR;         // RGB of current pixel
    
    // Intermediate signals (linking modules)
    wire        FB_DATA_VSG_DATA;   // link VGA_DATA to B_DATA
    wire        INTER_CLK;          // link DPR_CLK to A_CLK/B_CLK
    wire [14:0] FB_ADDR_VSG_ADDR;   // link VGA_ADDR to B_ADDR
    
    // Instantiate the sub-modules
    VGA_Sig_Gen vsg(
        .CLK(CLK),
        .RESET(RESET),
        .CONFIG_COLOURS(CONFIG_COLOURS),
        .VGA_DATA(FB_DATA_VSG_DATA),
        .DPR_CLK(INTER_CLK),
        .VGA_ADDR(FB_ADDR_VSG_ADDR),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS),
        .VGA_COLOUR(VGA_COLOUR)
    );
    
    VGA_Frame_Buffer fb(
        .A_CLK(INTER_CLK),
        .A_WE(A_WE),
        .A_ADDR(A_ADDR),
        .A_DATA_IN(A_DATA_IN),
        .A_DATA_OUT(FB_A_DATA),
        .B_CLK(INTER_CLK),
        .B_ADDR(FB_ADDR_VSG_ADDR),
        .B_DATA(FB_DATA_VSG_DATA)
    );
    
    // Signals local to this top level module
    reg [2:0]   DATA_GOT;           // indicate if address/data has been received
    reg [2:0]   DATA_GOT_nxt;
    reg [7:0]   A_ADDR_LOW;
    reg [6:0]   A_ADDR_HIGH;
    reg [7:0]   A_ADDR_LOW_nxt;
    reg [6:0]   A_ADDR_HIGH_nxt;
    reg         A_DATA_REG;
    reg         A_DATA_REG_nxt;
    reg [7:0]   COLORS;
    reg [7:0]   COLORS_nxt;         // new color setting
    
    reg [1:0]   WE_RESET;
    
    // Sequential logic for data bus input
    // assignment to frame buffer input
    always@(posedge CLK) begin
        if (RESET) begin
            A_ADDR <= 0;
            A_ADDR_HIGH <= 0;
            A_ADDR_LOW <= 0;
            A_DATA_IN <= 0;
            A_DATA_REG <= 0;
            A_WE <= 0;
            DATA_GOT <= 0;
            COLORS <= 0;
            WE_RESET <= 0;
        end
        // push to address/data inputs only if all set
        else if (DATA_GOT == 3'b111) begin
            A_ADDR[14:8] <= A_ADDR_HIGH;
            A_ADDR[7:0] <= A_ADDR_LOW;
            A_DATA_IN <= A_DATA_REG;
            A_WE <= 1;
            WE_RESET <= 1;
            // update non-volatile regs as usual        
            A_ADDR_HIGH <= A_ADDR_HIGH_nxt;
            A_ADDR_LOW <= A_ADDR_LOW_nxt;
            A_DATA_REG <= A_DATA_REG_nxt;
            DATA_GOT <= DATA_GOT_nxt;
            COLORS <= COLORS_nxt;
        end
        // keep WE on for 4 cycles, otherwise off
        else begin
            if ((WE_RESET > 0) && (WE_RESET < 4)) begin
                WE_RESET <= WE_RESET + 1'b1;
                A_WE <= 1;
            end
            else begin
                WE_RESET <= 0;
                A_WE <= 0;
            end
            A_ADDR_HIGH <= A_ADDR_HIGH_nxt;
            A_ADDR_LOW <= A_ADDR_LOW_nxt;
            A_DATA_REG <= A_DATA_REG_nxt;
            DATA_GOT <= DATA_GOT_nxt;
            COLORS <= COLORS_nxt;
        end
    end

    // Combinatorial logic for data bus input
    // assignment to frame buffer input:
    // B0: horizontal address
    // B1: vertical address (7 LSBs)
    // B2: data bit (LSB) or color bits
    always@* begin
        A_ADDR_LOW_nxt = A_ADDR_LOW;
        A_ADDR_HIGH_nxt = A_ADDR_HIGH;
        A_DATA_REG_nxt = A_DATA_REG;
        COLORS_nxt = COLORS;
        DATA_GOT_nxt = DATA_GOT;
        case(BUS_ADDR)
            B0: begin
                    if (!DATA_GOT[0]) begin
                        A_ADDR_LOW_nxt  = BUS_DATA;
                        DATA_GOT_nxt[0] = 1'b1;
                    end
                end
            B1: begin
                    if (!DATA_GOT[1]) begin
                        A_ADDR_HIGH_nxt = BUS_DATA[6:0];
                        DATA_GOT_nxt[1] = 1'b1;
                    end
                end
            B2: begin
                    // if waiting for last, it is data value
                    if (DATA_GOT == 3'b011) begin
                        A_DATA_REG_nxt = BUS_DATA[0];
                        DATA_GOT_nxt[2] = 1'b1;
                    end
                    // if not, it is color value
                    else COLORS_nxt = BUS_DATA;
                end
            default: DATA_GOT_nxt = (DATA_GOT == 3'b111) ? 3'b000 : DATA_GOT;
        endcase
    end
    
    // Flip foreground and background colors as needed
    assign CONFIG_COLOURS = {~COLORS, COLORS};
    
    // Convert the 8 color bits to 12 VGA output bits
    // Extend the MSBs of each colour
    assign VGA_OUT = {VGA_COLOUR[7],VGA_COLOUR[7:5],VGA_COLOUR[4],VGA_COLOUR[4:2],VGA_COLOUR[1],VGA_COLOUR[1],VGA_COLOUR[1:0]};
    
endmodule
