`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis
// 
// Create Date: 07.03.2017
// Design Name: Arithmetic Logic Unit
// Module Name: ALU
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The ALU module, specifying all the arithmetic operations
//              invokable by the processor instruction set.
// 
// Dependencies: None
// 
// Revision: 1.1 - added A mod B, A or B, not A, not B operations
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ALU(
	//standard signals
	input          CLK,
	input          RESET,
	//I/O
	input  [7:0]   IN_A,
	input  [7:0]   IN_B,
	input  [3:0]   ALU_Op_Code,
	output [7:0]   OUT_RESULT
);

reg [7:0] Out;
//Arithmetic Computation
always@(posedge CLK) begin
	if(RESET)
		Out <= 0;
	else begin 
		case (ALU_Op_Code)
			//Maths Operations
			//Add A + B
			4'h0: Out <= IN_A + IN_B;
			//Subtract A - B
			4'h1: Out <= IN_A - IN_B;
			//Multiply A * B
			4'h2: Out <= IN_A * IN_B;
			//Shift Left A << 1
			4'h3: Out <= IN_A << 1;
			//Shift Right A >> 1
			4'h4: Out <= IN_A >> 1;
			//Increment A+1
			4'h5: Out <= IN_A + 1'b1;
			//Increment B+1
			4'h6: Out <= IN_B + 1'b1;
			//Decrement A-1
			4'h7: Out <= IN_A - 1'b1;
			//Decrement B-1
			4'h8: Out <= IN_B - 1'b1;
			// In/Equality Operations
			//A == B
			4'h9: Out <= (IN_A == IN_B) ? 8'h01 : 8'h00;
			//A > B
			4'hA: Out <= (IN_A > IN_B) ? 8'h01 : 8'h00;
			//A < B
			4'hB: Out <= (IN_A < IN_B) ? 8'h01 : 8'h00;
			//A mod B
			4'hC: Out <= IN_A % IN_B;
			//A or B
			4'hD: Out <= IN_A | IN_B;
			//Not A
			4'hE: Out <= ~IN_A;
			//Not B
			4'hF: Out <= ~IN_B; 
			//Default A
			default: Out <= IN_A;
		endcase
	end
end

assign OUT_RESULT = Out;

endmodule
