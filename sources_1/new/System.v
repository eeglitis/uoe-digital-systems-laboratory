`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Edinburgh
// Engineer: Edgars Eglitis, Justas Lukosiunas, Deividas Krukauskas
// 
// Create Date: 07.03.2017
// Design Name: System top-level module
// Module Name: System
// Project Name: Processor
// Target Devices: Digilent Basys3
// Tool Versions: Vivado 2015.2
// Description: The System top-level module, instantiating all the relevant
//              processing and peripheral modules.
// 
// Dependencies: Processor
//               RAM
//               ROM
//               Timer
//               VGA_Interface
//               IRTransmitter_peripheral
//               Peripheral_Mouse_Interface
//               Seven_seg_peripheral
//               LED_Peripheral
// 
// Revision: 1.2 - All modules added
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

module System(
    input           CLK,
    input           RESET,
    input [3:0]     CAR_SELECT,
    // Mouse inouts
    inout PS2_CLK,
    inout PS2_DATA,
    // VGA outputs
    output  [11:0]  VGA_OUT,
    output          VGA_HS,
    output          VGA_VS,
    // IR output
    output          IR_LED,
    output [3:0] SEG_SELECT,
    output [7:0] DEC_OUT,
    output [7:0] LED_OUT 
    );
    
    // Link all the modules together
    wire    [7:0]   BUS_DATA;
    wire    [7:0]   BUS_ADDR;
    wire            BUS_WE;
    wire    [7:0]   ROM_DATA;
    wire    [7:0]   ROM_ADDRESS;
    wire    [1:0]   BUS_INTERRUPT_RAISE;
    wire    [1:0]   BUS_INTERRUPT_ACK;
        
    Processor Proc0(
        //Standard Signals
        .CLK(CLK),
        .RESET(RESET),
        //BUS Signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE),
        // ROM signals
        .ROM_ADDRESS(ROM_ADDRESS),
        .ROM_DATA(ROM_DATA),
        // INTERRUPT signals
        .BUS_INTERRUPT_RAISE(BUS_INTERRUPT_RAISE),
        .BUS_INTERRUPT_ACK(BUS_INTERRUPT_ACK)
    );    
    
    RAM RAM0(
        //standard signals
        .CLK(CLK),
        //BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE)
    );
    
    ROM ROM0(
        //standard signals
        .CLK(CLK),
        //BUS signals
        .DATA(ROM_DATA),
        .ADDR(ROM_ADDRESS)
    );
    
    Timer Timer0(
        //standard signals
        .CLK(CLK),
        .RESET(RESET),
        //BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE),
        .BUS_INTERRUPT_RAISE(BUS_INTERRUPT_RAISE[1]),
        .BUS_INTERRUPT_ACK(BUS_INTERRUPT_ACK[1])
    );
    
    VGA_Interface VGA0(
        //standard signals
        .CLK(CLK),
        .RESET(RESET),
        //BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE),
        //VGA signals
        .VGA_OUT(VGA_OUT),
        .VGA_HS(VGA_HS),
        .VGA_VS(VGA_VS)
    );
    
    IRTransmitter_peripheral IRTransmitter0(
        // Standard signals
        .CLK(CLK),
        .RESET(RESET),
        .CAR_SELECT(CAR_SELECT),
        // BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        //.SEG_SELECT(SEG_SELECT),
        // IR output
        .IR_LED(IR_LED)
    ); 
    
    Peripheral_Mouse_Interface PMI(
        //BUS signals
        .CLK(CLK),
        .RESET(RESET),
        //BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE),
        .BUS_INTERRUPT_RAISE(BUS_INTERRUPT_RAISE[0]),
        .BUS_INTERRUPT_ACK(BUS_INTERRUPT_ACK[0]),
        .PS2_CLK(PS2_CLK),
        .PS2_DATA(PS2_DATA)   
    );
    
    Seven_seg_peripheral SSP(
        //BUS signals
        .CLK(CLK),
        .RESET(RESET),
        //BUS signals
        .BUS_DATA(BUS_DATA),
        .BUS_ADDR(BUS_ADDR),
        .BUS_WE(BUS_WE),
        .SEG_SELECT(SEG_SELECT),
        .DEC_OUT(DEC_OUT) 
    );
        
    LED_Peripheral LEDP(
            //BUS signals
            .CLK(CLK),
            .RESET(RESET),
            //BUS signals
            .BUS_DATA(BUS_DATA),
            .BUS_ADDR(BUS_ADDR),
            .BUS_WE(BUS_WE),
            .LED_OUT(LED_OUT)
    );

endmodule
