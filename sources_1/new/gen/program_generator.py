#!/usr/bin/python

##########################################################
################ PROGRAM GENERATOR SCRIPT ################
##########################################################
# This script has two purposes:
# a) fill the RAM with user defined data, linked to
#    variable names. The addresses of this data are
#    assigned dynamically.
# b) provide multiple higher levels of abstraction
#    for generating the Assembly code responsible
#    for the creation of the car control system.
#    This allows to specify the program operation
#    in readable English, instead of Assembly.
##########################################################

import math
import sys

verbose = 1 # enables printing PC positions

###############################################################################
###########################     RAM BUILDING
###############################################################################

output = open("../RAM.txt", 'w')

# debug mode initializer (to paste code directly into RAM.v)
debug = 0
if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
	debug = 1
	print 'DEBUG MODE - for copying code into RAM.v directly'

# Define a class entry for RAM address and value
class RAM_ENTRY(object):
    def __init__(self, VAL, ADDR):
        self.VAL  = VAL
	self.ADDR = ADDR

lines = 0
# Function to assign a new RAM entry and address to each variable
# and print to the RAM file
def add_entry(val):
	global lines
	entry = RAM_ENTRY(val, lines)

        if debug:
                output.write('Mem[{}] = 8\'b'.format(lines))
        output.write(str(format(entry.VAL, '08b')))
        if debug:
                output.write(';')
        output.write('\n')
	lines += 1

	return entry

##########################################################
#################     RAM VALUE DEFINITIONS START

ZERO  = add_entry(0) # holds 0
ONE   = add_entry(1) # holds 1
TWO   = add_entry(2) # holds 2
FOUR  = add_entry(4) # holds 4
EIGHT = add_entry(8) # holds 8

X_CURR = add_entry(0)   # current X counter / old mouse X position
Y_CURR = add_entry(0)   # current Y counter / old mouse Y position
X_1    = add_entry(50)  # first X border (200/4 = 50)
Y_1    = add_entry(40)  # first Y border (160/4 = 40)
X_2    = add_entry(110) # second X border (440/4 = 110)
Y_2    = add_entry(80)  # second Y border (320/4 = 80)
X_MAX  = add_entry(160) # max X value (640/4 = 160)
Y_MAX  = add_entry(120) # max Y value (480/4 = 120)

MOUSE_X      = add_entry(0) # X position of mouse pointer
MOUSE_Y      = add_entry(0) # Y position of mouse pointer
MOUSE_STATUS = add_entry(8) # mouse status value, updated during click
DIRECTION    = add_entry(0) # assembles mouse X and Y positions to make car direction

PIXEL        = add_entry(0) # holds current pixel value (A_DATA)
COLOUR       = add_entry(0) # holds current colour value
PATTERN_DONE = add_entry(0) # holds whether the full pattern is drawn

#################     RAM VALUE DEFINITIONS END
##########################################################

# Check that there is still space in the RAM
if lines > 128:
	print "\nRAM full! Stopping program generator.\n"
	output.close()
	sys.exit()
else: # fill remaining locations
        for x in range(lines, 128):
                if debug:
                        output.write('Mem[{}] = 8\'b'.format(x))
                output.write(str(format(0, '08b')))
                if debug:
                        output.write(';')
                output.write('\n')
        print "\nRAM generation successful!\n"

output.close()
    
###############################################################################
###########################     ROM ASSEMBLY BUILDING
###############################################################################

output = open("assembly.txt", 'w')
PC = 0 # program counter

##########################################################
#################     ADDRESS DEFINITIONS
##########################################################

# ROM addresses
BASE_PC            = 0x00 # always points to start
BORDER_PIXEL_PC    = 0x00 # calculate if pixel is at border, set based on PC
LOOP_START_PC      = 0x00 # pattern generator loop address, set based on PC

MOUSE_INTERRUPT_PC = 0x00 # mouse interrupt target address, set based on PC

# VGA bus addresses
VGA_ADDR0 = 0xB0
VGA_ADDR1 = 0xB1
VGA_ADDR2 = 0xB2

# IR bus address
IR_ADDR = 0x90

# Mouse addresses
MOUSE_ADDR0 = 0xA0
MOUSE_ADDR1 = 0xA1
MOUSE_ADDR2 = 0xA2

# 7 Segment display addresses
SSEG_ADDR0 = 0xD0
SSEG_ADDR1 = 0xD1

# LED address
LED_ADDR = 0xC0

##########################################################
#################     LEVEL 1 FUNCTIONS
##########################################################

################# MEMORY OPERATIONS #################

def load_a(address):
        global PC
        PC += 2 
        output.write('LOAD A {} {}'.format(hex(address), '\n'))

def load_b(address):
        global PC
        PC += 2 
        output.write('LOAD B {} {}'.format(hex(address), '\n'))

#################

def store_a(address):
        global PC
        PC += 2 
        output.write('STORE A {} {}'.format(hex(address), '\n'))

def store_b(address):
        global PC
        PC += 2 
        output.write('STORE B {} {}'.format(hex(address), '\n'))

################# ALU OPERATIONS #################

def alu_a_add_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_ADD_B {}'.format(register, '\n'))         

def alu_a_sub_b(register):
        global PC
        PC += 1  
        output.write('ALU {} A_SUB_B {}'.format(register, '\n'))

def alu_a_mul_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_MULT_B {}'.format(register, '\n'))

#################

def alu_a_shift_l(register):
        global PC
        PC += 1 
        output.write('ALU {} SHIFT_A_L {}'.format(register, '\n'))      

def alu_a_shift_r(register):
        global PC
        PC += 1 
        output.write('ALU {} SHIFT_A_R {}'.format(register, '\n'))
         
#################

def alu_incr_a(register):
        global PC
        PC += 1 
        output.write('ALU {} INCR_A {}'.format(register, '\n'))

def alu_incr_b(register):
        global PC
        PC += 1  
        output.write('ALU {} INCR_B {}'.format(register, '\n'))

#################

def alu_decr_a(register):
        global PC
        PC += 1 
        output.write('ALU {} DECR_A {}'.format(register, '\n'))

def alu_decr_b(register):
        global PC
        PC += 1 
        output.write('ALU {} DECR_B {}'.format(register, '\n'))

#################

def alu_a_eq_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_EQ_B {}'.format(register, '\n'))

def alu_a_gt_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_GT_B {}'.format(register, '\n'))

def alu_a_lt_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_LT_B {}'.format(register, '\n'))

def alu_a_mod_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_MOD_B {}'.format(register, '\n'))

def alu_a_or_b(register):
        global PC
        PC += 1 
        output.write('ALU {} A_OR_B {}'.format(register, '\n'))

def alu_not_a(register):
        global PC
        PC += 1 
        output.write('ALU {} NOT_A {}'.format(register, '\n'))

def alu_not_b(register):
        global PC
        PC += 1 
        output.write('ALU {} NOT_B {}'.format(register, '\n'))

################# BRANCHING #################
         
def branch_a_eq_b(address):
        global PC
        PC += 2
        output.write('BRANCH_A_EQ_B {} {}'.format(hex(address), '\n'))

def branch_a_gt_b(address):
        global PC
        PC += 2  
        output.write('BRANCH_A_GT_B {} {}'.format(hex(address), '\n'))

def branch_a_lt_b(address):
        global PC
        PC += 2  
        output.write('BRANCH_A_LT_B {} {}'.format(hex(address), '\n'))

#################

def goto(address): 
        global PC
        PC += 2 
        output.write('GOTO {} {}'.format(hex(address), '\n'))

#################

def goto_idle():
        global PC
        PC += 1  
        output.write('GOTO_IDLE {}'.format('\n'))

################# FUNCTION CALLS #################

def funcall(address):
        global PC
        PC += 2  
        output.write('FUNCALL {} {}'.format(hex(address), '\n'))

def func_return():
        global PC
        PC += 1  
        output.write('RETURN {}'.format('\n'))  

################# DEREFERENCING #################

def deref(register):
        global PC
        PC += 1  
        output.write('DEREF {} {}'.format(register, '\n'))

################# NOP #################

def nop():
        global PC
        PC += 1  
        output.write('NOP \n')

##########################################################
#################     LEVEL 2 FUNCTIONS
##########################################################

# Check if X is border, set pixel to 1 if so
def is_x_border():

        if verbose:
                print '{}\tX_BORDER'.format(PC)
        
        load_a(X_CURR.ADDR)    # a = x
 	load_b(X_1.ADDR)       # b = X_1
	branch_a_eq_b(PC + 10) # if (a==b) branch
	load_b(X_2.ADDR)       # b = X_2
	branch_a_eq_b(PC + 6)  # if (a==b) branch
	load_a(ZERO.ADDR)      # x!=border here -> a = 0
	goto(PC + 4)           # branch to store
	load_a(ONE.ADDR)       # x==border here -> a = 1
        store_a(PIXEL.ADDR)    # pixel = a (base pixel setting)

# Check if Y is border, set pixel to 1 if so
def is_y_border():

        if verbose:
                print '{}\tY_BORDER'.format(PC)

        load_a(Y_CURR.ADDR)   # a = y
 	load_b(Y_1.ADDR)      # b = Y_1
	branch_a_eq_b(PC + 8) # if (a==b) branch
	load_b(Y_2.ADDR)      # b = Y_2
	branch_a_eq_b(PC + 4) # if (a==b) branch
	goto(PC + 6)          # y!=border, skip because might overwrite x border
	load_a(ONE.ADDR)      # y==border here -> a = 1
        store_a(PIXEL.ADDR)   # pixel = 1 (overwrite only Y borders)  

# Write the X address, Y address, and pixel values to the VGA
def write_to_vga():
        
        if verbose:
                print '{}\tWRITE_TO_VGA'.format(PC)

        load_a(X_CURR.ADDR) # a = x
        store_a(VGA_ADDR0)  # 0xB0 = x

        load_a(Y_CURR.ADDR) # a = y
        store_a(VGA_ADDR1)  # 0xB1 = y

        load_a(PIXEL.ADDR)  # a = pixel
        store_a(VGA_ADDR2)  # 0xB2 = pixel 

# Update the X address
# Store whether X is reset to 0 in B
# Assumes update_y() is called afterwards
def update_x():

        if verbose:
                print '{}\tX_UPDATE'.format(PC)

        load_a(X_CURR.ADDR) # a = x_curr
        load_b(X_MAX.ADDR)  # b = x_max
        alu_incr_a('A')     # a = x_curr + 1
        alu_a_mod_b('A')    # a = x % x_max
        store_a(X_CURR.ADDR)# x_curr = x % x_max

	load_b(ZERO.ADDR)   # b = 0
	alu_a_eq_b('B')	    # b = x_reset

# Check if X has been reset
# If so, update the Y address
# Store whether Y is reset to 0 in PATTERN_DONE_ADDR
def update_y():

        if verbose:
                print '{}\tY_UPDATE'.format(PC)

        Y_SKIP_PC = PC + 17 # ROM address after this procedure

        load_a(ZERO.ADDR)         # a = 0 (b = x_reset)
        branch_a_eq_b(Y_SKIP_PC)  # if x not reset, do not increment

        load_a(Y_CURR.ADDR) # a = y_curr
        load_b(Y_MAX.ADDR)  # b = y_max
        alu_incr_a('A')     # a = y_curr + 1
        alu_a_mod_b('A')    # a = y % y_max
        store_a(Y_CURR.ADDR)# y_curr = y % y_max 

	load_b(ZERO.ADDR)          # b = 0
	alu_a_eq_b('B')	           # b = (y == 0)
        store_b(PATTERN_DONE.ADDR) # pattern_done = b

# Update VGA mouse position from mouse peripheral
# Also push position to 7-segment display
def update_mouse():

        if verbose:
                print '{}\tUPDATE_MOUSE'.format(PC)

	load_a(MOUSE_ADDR0)   # a = mouse_x_new
	store_a(MOUSE_X.ADDR) # mouse_x = mouse_x_new
	store_a(SSEG_ADDR0)   # sseg_0 = mouse_x_new

	# Mouse Y position starts at bottom, so need to invert
	load_a(Y_MAX.ADDR)    # a = y_max
	load_b(MOUSE_ADDR1)   # b = mouse_y_new
	alu_a_sub_b('A')      # a = y_max - mouse_y_new
	store_a(MOUSE_Y.ADDR) # mouse_y = y_max - mouse_y_new
	store_a(SSEG_ADDR1)   # sseg_1 = mouse_y

# Draw the mouse pointer
def draw_mouse():

        if verbose:
                print '{}\tDRAW_MOUSE'.format(PC)

        load_a(MOUSE_X.ADDR) # a = mouse_x
        store_a(VGA_ADDR0)   # 0xB0 = mouse_x

        load_a(MOUSE_Y.ADDR) # a = mouse_y
        store_a(VGA_ADDR1)   # 0xB1 = mouse_y

        load_a(ONE.ADDR)   # a = 1
        store_a(VGA_ADDR2) # 0xB2 = 1 

# Overwrite old mouse location values with new location
def update_old_pointer():

        if verbose:
                print '{}\tUPDATE_OLD_POINTER'.format(PC)

	load_a(MOUSE_X.ADDR)
	store_a(X_CURR.ADDR)
	load_a(MOUSE_Y.ADDR)
	store_a(Y_CURR.ADDR)

# Invert the colors if any mouse button clicked
def check_click():

        if verbose:
                print '{}\tCHECK_CLICK'.format(PC)

	load_a(MOUSE_STATUS.ADDR) # a = mouse_status
	load_b(MOUSE_ADDR2)       # b = mouse_status_new
	branch_a_eq_b(PC+13)      # if (mouse_status==mouse_status_new)
				  # no click, skip
	branch_a_gt_b(PC+9)       # if (mouse_status>mouse_status_new)
				  # button release, only update status

	load_a(COLOUR.ADDR)  # a = colour
	alu_not_a('A')       # a = ~a
	store_a(COLOUR.ADDR) # colour = a
	store_a(VGA_ADDR2)   # 0xB2 = a (set colour)

	store_b(MOUSE_STATUS.ADDR) # mouse_status = mouse_status_new

# Initialize the IR direction with X region (left/middle/right)
def find_x_region():

        if verbose:
                print '{}\tFIND_X_REGION'.format(PC)

	load_a(MOUSE_X.ADDR) # a = mouse_x
	load_b(X_1.ADDR)     # b = X_1
	branch_a_lt_b(PC+10) # if (mouse_x<X_1) branch to set left

	# mouse_x >= X_1 here
	load_b(X_2.ADDR)     # b = X_2
	branch_a_lt_b(PC+10) # if (mouse_x<X_2) skip - do not set

	# mouse_x >= X_2 here, handle
	load_a(ONE.ADDR) # set right - a = 1
	goto(PC+4)       # branch to store

	# mouse_x < X_1 handler
	load_a(TWO.ADDR)        # set left - a = 2

	store_a(DIRECTION.ADDR) # direction = a

# Increment the IR direction with Y region (forward/middle/backward)
def find_y_region():

        if verbose:
                print '{}\tFIND_Y_REGION'.format(PC)

	load_a(MOUSE_Y.ADDR) # a = mouse_y
	load_b(Y_1.ADDR)     # b = Y_1
	branch_a_lt_b(PC+12) # if (mouse_y<Y_1) branch to set forward

	# mouse_y >= Y_1 here
	load_b(Y_2.ADDR)     # b = Y_2
	branch_a_lt_b(PC+15) # if (mouse_y<Y_2) skip - do not set

	# mouse_y >= Y_2 here, handle
	load_a(DIRECTION.ADDR) # a = direction
	load_b(FOUR.ADDR)      # set backward - b = 4
	goto(PC+6)             # branch to increment

	# mouse_y < Y_1 handler
	load_a(DIRECTION.ADDR) # a = direction
	load_b(EIGHT.ADDR)     # set forward - b = 8

	# increment direction
	alu_a_add_b('A')        # a = direction + b
	store_a(DIRECTION.ADDR) # direction = a

# Write the movement direction to the IR and LED, then reset it
def push_direction():

        if verbose:
                print '{}\tPUSH_DIRECTION'.format(PC)

	load_a(DIRECTION.ADDR) # a = direction
	store_a(IR_ADDR)       # IR_ADDR = direction
	store_a(LED_ADDR)      # LED_ADDR = direction

	load_a(ZERO.ADDR)       # a = 0
	store_a(DIRECTION.ADDR) # direction = 0

##########################################################
#################     LEVEL 3 FUNCTIONS
##########################################################

# Function to determine if pixel is border
# and write its value (1 if border, 0 if not)
def write_border_pixel():

	global BORDER_PIXEL_PC
        BORDER_PIXEL_PC = PC

	is_x_border()  # set pixel if at X border
	is_y_border()  # set pixel if at Y border
	write_to_vga() # write MOUSE_X, MOUSE_Y and PIXEL to VGA

	func_return()

# Loop to draw the initial pattern
def write_vga_pattern():
        
        if verbose:
                print '{}\t--VGA PATTERN START--'.format(PC)

        load_a(COLOUR.ADDR) # a = colour
        store_a(VGA_ADDR2)  # 0xB2 = a (set colour)

        global LOOP_START_PC
        LOOP_START_PC = PC

	funcall(BORDER_PIXEL_PC) # set pixel if it is border
        update_x() # x = (x+1) % x_max
        update_y() # y = (y+1) % y_max

        load_a(PATTERN_DONE.ADDR) # a = vga_pattern_done
        load_b(ONE.ADDR)          # b = 1

        branch_a_eq_b(PC + 4) # jump over return to base address if a == b
                              # (breaks the loop once pattern written)
        goto(LOOP_START_PC)   # else start the loop over	

        if verbose:
                print '{}\t--VGA PATTERN END--'.format(PC)
                print '{}\tIDLE'.format(PC)

	# after this point, X_CURR and Y_CURR are used only for old mouse location

        goto_idle() # function completed, idle 

# Mouse interrupt handler (at 0xFF)
def mouse_response():

	global MOUSE_INTERRUPT_PC
	MOUSE_INTERRUPT_PC = PC
        if verbose:
                print '{}\t--MOUSE INTERRUPT START--'.format(PC)

	update_mouse()           # obtain new pointer values
	funcall(BORDER_PIXEL_PC) # erase old pointer
	draw_mouse()	         # draw new pointer
	update_old_pointer()     # old pointer = new pointer

	check_click() # invert color if clicked

	find_x_region()  # get x direction
	find_y_region()  # increment with y direction
	push_direction() # write full direction, then reset it

        if verbose:
                print '{}\t--MOUSE INTERRUPT END--'.format(PC)
                print '{}\tIDLE'.format(PC)

        goto_idle() # function completed, idle 

# Fill rest of the ROM with NOP instructions
# Add redirect to mouse interrupt in the end
def insert_interrupts():
        global PC
        if verbose:
                print '{}\tNOP_INSERT'.format(PC)

        # Fill in the rest of the ROM with NOPs
        for x in range(254 - PC):
                nop()

        # No timer interrupt, address effectively irrelevant
	PC += 1
        output.write(str(format(BASE_PC, '08b')))
        output.write('\n')

        # Insert the mouse interrupt address into ROM[FF]
	PC += 1
        output.write(str(format(MOUSE_INTERRUPT_PC, '08b')))
        output.write('\n')              

###############################################################################
###########################     MAIN ROM INVOCATION POINT
###############################################################################

if verbose:
	print 'PC positions:'

goto(PC+49) # skip the border pixel function (47) plus the goto itself (2)
write_border_pixel()
write_vga_pattern()
mouse_response()
insert_interrupts()

# Check that there is still space in the ROM
if PC > 256:
	print "\nROM full! Stopping program generator.\n"
	output.close()
	sys.exit()
else:
	print "\nAssembly code generation successful!\n"

output.close()


