#!/usr/bin/python

##########################################################
#################### ASSEMBLER SCRIPT ####################
##########################################################
# This script converts an Assembly-formatted input file
# into a bytecode representation of the ROM.
# It also warns if the number of byte instructions
# exceeds the maximum size of the ROM (256 entries).
##########################################################

import math
import sys

# Debug mode initializer (to paste code directly into ROM.v)
debug = 0
if len(sys.argv) > 1 and int(sys.argv[1]) == 1:
	debug = 1
	print 'DEBUG MODE - for copying code into ROM.v directly'

input_file = open("assembly.txt", 'r')
output_file = open("../ROM.txt", 'w')

line_number = 0 # counts lines to see if there is enough space

################# tokenize each instruction

for line in input_file:
	line.rstrip('\n')
        tokens = line.split()

        if len(tokens) < 1:
                continue

##########################################################
#################     LOAD INSTRUCTION
##########################################################

        if tokens[0] == 'LOAD':

                ################# print the LOAD PART AND TARGET REGISTER

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1        
                if tokens[1] == 'A':
                        output_file.write(str(format(0, '08b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(1, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

                ################# print SOURCE ADDRESS

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1        
                address = int(tokens[2], 0)
                output_file.write(str(format(address, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     STORE INSTRUCTION
##########################################################

        elif tokens[0] == 'STORE':

                ################# print the LOAD PART and TARGET REGISTER

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                if   tokens[1] == 'A':
                        output_file.write(str(format(2, '08b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(3, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

                ################# print TARGET ADDRESS

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                address = int(tokens[2], 0)
                output_file.write(str(format(address, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     ALU INSTRUCTION
##########################################################

        elif tokens[0] == 'ALU':

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1

                ################# print ALU OPERATION

                ##### ARITHMETICS
                if   tokens[2] == 'A_ADD_B':
                        output_file.write(str(format(0, '04b')))
                elif tokens[2] == 'A_SUB_B':
                        output_file.write(str(format(1, '04b')))
                elif tokens[2] == 'A_MULT_B':
                        output_file.write(str(format(2, '04b')))
                elif tokens[2] == 'SHIFT_A_L':
                        output_file.write(str(format(3, '04b')))
                elif tokens[2] == 'SHIFT_A_R':
                        output_file.write(str(format(4, '04b')))

                ##### INCREMENTS
                elif tokens[2] == 'INCR_A':
                        output_file.write(str(format(5, '04b')))
                elif tokens[2] == 'INCR_B':
                        output_file.write(str(format(6, '04b')))
                elif tokens[2] == 'DECR_A':
                        output_file.write(str(format(7, '04b')))
                elif tokens[2] == 'DECR_B':
                        output_file.write(str(format(8, '04b')))

                ##### LOGIC
                elif tokens[2] == 'A_EQ_B':
                        output_file.write(str(format(9, '04b')))
                elif tokens[2] == 'A_GT_B':
                        output_file.write(str(format(10, '04b')))
                elif tokens[2] == 'A_LT_B':
                        output_file.write(str(format(11, '04b')))
                elif tokens[2] == 'A_MOD_B':
                        output_file.write(str(format(12, '04b')))
                elif tokens[2] == 'A_OR_B':
                        output_file.write(str(format(13, '04b')))	
                elif tokens[2] == 'NOT_A':
                        output_file.write(str(format(14, '04b')))
                elif tokens[2] == 'NOT_B':
                        output_file.write(str(format(15, '04b')))	


                ################# print OPCODE
                if   tokens[1] == 'A':
                        output_file.write(str(format(4, '04b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(5, '04b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     BRANCHING INSTRUCTIONS
##########################################################

        elif tokens[0][0:6] == 'BRANCH':

                ################# print OPCODE

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1 
                if tokens[0][6:] == '_A_EQ_B':
                        output_file.write(str(format(150, '08b')))
                elif tokens[0][6:] == '_A_GT_B':
                        output_file.write(str(format(166, '08b')))
                elif tokens[0][6:] == '_A_LT_B':
                        output_file.write(str(format(182, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

                ################# print the BRANCH ADDRESS

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                address = int(tokens[1], 0)
                output_file.write(str(format(address, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     GOTO INSTRUCTION
##########################################################

        elif tokens[0] == 'GOTO':
                
                ################# print GOTO OPCODE

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(str(format(7, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

                ################# print the GOTO ADDRESS

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                address = int(tokens[1], 0)
                output_file.write(str(format(address, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     GOTO IDLE INSTRUCTION
##########################################################

        elif tokens[0] == 'GOTO_IDLE':

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(str(format(8, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     FUNCTION CALL INSTRUCTION
##########################################################

        elif tokens[0] == 'FUNCALL':

                ################# print FUNCALL OPCODE

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(str(format(9, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

                ################# print the FUNCTION ADDRESS

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                address = int(tokens[1], 0)
                output_file.write(str(format(address, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     RETURN INSTRUCTION
##########################################################

        elif tokens[0] == 'RETURN':

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(str(format(10, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n") 

##########################################################
#################     DEREFERENCE INSTRUCTIONS
##########################################################
        
        elif tokens[0] == 'DEREF':

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                if tokens[1] == 'A':
                        output_file.write(str(format(11, '08b')))
                elif tokens[1] == 'B':
                        output_file.write(str(format(12, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     NO-OP INSTRUCTIONS
##########################################################

        elif tokens[0] == 'NOP':

                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(str(format(0xff, '08b')))

                if debug:
                        output_file.write(';')
                output_file.write("\n")

##########################################################
#################     INTERRUPT ADDRESSES
##########################################################

        else:
                if debug:
                        output_file.write('ROM[{}] = 8\'b'.format(line_number))

                line_number += 1
                output_file.write(tokens[0]) # simply re-print

                if debug:
                        output_file.write(';')
                output_file.write("\n")

        # finally, check that there is still space in the ROM
        if line_number > 256:
                print "\nROM full! Stopping assembler.\n"
                break 

if line_number == 256:
        print "\nROM generation successful!\n"

input_file.close()
output_file.close()


