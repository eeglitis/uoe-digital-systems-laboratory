`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: The University of Edinburgh
// Engineer: Justas Lukosiunas
// 
// Create Date: 17.02.2017 18:21:17
// Design Name: Mouse_interface
// Module Name: Peripheral_Mouse_Interface
// Project Name: Digital Systems Laboratory
// Target Devices: BASYS 3
// Tool Versions: 
// Description: This module wraps up previously implemented Mouse interface
//              in order to transmit the mouse X and Y coordinates and the 
//              status byte when the appropriate addresses are triggered on the
//              data address bus: 0xA0 - X coordinate, 0xA1 - Y coordinate, 0xA2 -
//              status byte.
// 
// Dependencies: Mouse Transceiver
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module Peripheral_Mouse_Interface(
    inout  [7:0]   BUS_DATA,
    input  [7:0]   BUS_ADDR,
    input          BUS_WE,
    output         BUS_INTERRUPT_RAISE,
    input          BUS_INTERRUPT_ACK,
    input          CLK,
    input          RESET,
    inout          PS2_CLK,
    inout          PS2_DATA   
);

    //Wires for connecting the mouse data buses
    wire [7:0] x;
    wire [7:0] y;
    wire [3:0] status;
    wire SendInterrupt;
    
    //defining the addresses for the 3 bytes of mouse data
    localparam
    X_ADDR = 8'hA0, 
    Y_ADDR = 8'hA1, 
    STATUS_ADDR = 8'hA2;
    
    MouseTransceiver MT(
            //Standard Inputs
            .RESET(RESET),
            .CLK(CLK),
            //IO - Mouse side
            .CLK_MOUSE(PS2_CLK),
            .DATA_MOUSE(PS2_DATA),
            // Mouse data information
            .MouseStatus(status),
            .MouseX(x),
            .MouseY(y),
            .LED_OUT(LED_OUT),
            .SEND_INTERRUPT(SendInterrupt)
    );
    
    //interrupt handling
    reg Interrupt;
    always@(posedge CLK) begin
        if(RESET)
            Interrupt <= 1'b0;
        else if(BUS_INTERRUPT_ACK)
            Interrupt <= 1'b0;
        else
            //taking the interrupt signal from the MouseTransceiver
            //it is sent after every pulse of successfully transmitted mouse data
            Interrupt <= SendInterrupt;
    end
    
    assign BUS_INTERRUPT_RAISE = Interrupt;
    
    
    reg [7:0] BusDataValueToWrite; 
    reg WriteEnable;
    
    //Data bus handling
    always@(posedge CLK) begin
        if (RESET) begin
            BusDataValueToWrite <= 8'h00;
            WriteEnable <= 1'b0;
        end
        else if (BUS_ADDR == X_ADDR) begin
            BusDataValueToWrite <= x;
            WriteEnable <= 1'b1;
        end
        else if (BUS_ADDR == Y_ADDR) begin
            BusDataValueToWrite <= y;
            WriteEnable <= 1'b1;
        end
        else if (BUS_ADDR == STATUS_ADDR) begin
            BusDataValueToWrite <= status;
            WriteEnable <= 1'b1;
        end
        else
            WriteEnable <= 1'b0;
    end
    
    //assigning the local registers to the output
    assign BUS_DATA = (WriteEnable) ? BusDataValueToWrite : 8'hZZ;
        
endmodule
